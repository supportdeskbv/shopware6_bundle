# Bundle Plugin for Shopware 6

## Install

```bash
composer require "supportdesk/bundle-product"
```

## The plugin

### Purpose
An easy way to assign sub products to main products.
It should be (somewhat) comparable with [Shopware 5 bundle](https://store.shopware.com/swagbundle/bundle.html) extension
However this example shows combined discounts, while in this case we will always combine these products.

- Gives options to set a minimal/maximum qty per subproduct to be ordered directly with the current product.
- This can also be seen as comparable to variation products.
- The products are in the order as given in the backend
- When added to the cart it is shown as sub/associated products with qty and price
    - The main product should have an combined price.
- The bundle might be changed in the cart, but when someone adds another grouped with slightly different settings (e.g. one product differs) it should be added as a different product.

The main product is a container product that only contains the price and general information and should not be buyable singlely

**example**:
Main product:
  - Set of shirt and pants
Sub products:
  - Shirt in size xs, s, m, l, xl
  - pants in different lengths/widths

### API

Makes use of the default shopware api services.

- Adds an association for products.
- Association is accessible with 
```javascript
const criteria = new ProductCriteria();
criteria.addAssociation('bundles');
```

### Quickly add bundles to products
For adding a bundle through the command line the command that is used is:

```bash
./bin/console bundle:assign {main_id} {sub_id} {sub_id}
```

- the **main_id** is the product you want to have bundle products assigned to
- the **sub_id** is an array of product ids that are separated by a space. These will be assigned as the bundled products

The product id can be taken from the url (`admin#/sw/product/detail/{product_id}/base`)

### Todo
- Make a bundle have a discount option
- Maybe disable some functionality when bundle's are enabled/set.
- Make the bundleDemo command work

