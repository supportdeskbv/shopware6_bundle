<?php

namespace SupportDesk\BundleProduct\SalesChannelApi\Controller;

use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\Product\Configurator\ProductCombinationFinder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class BundleController extends AbstractController
{
    /**
     * @var ProductCombinationFinder
     */
    private $combinationFinder;

    public function __construct(ProductCombinationFinder $combinationFinder)
    {
        $this->combinationFinder = $combinationFinder;
    }

    /**
     * @RouteScope(scopes={"sales-channel-api"})
     * @Route("/sales-channel-api/v{version}/bundleproduct/{id}/options", name="sales-channel-api.action.bundleproduct.options", methods={"POST"})
     *
     * @param string              $id
     * @param Request             $request
     * @param SalesChannelContext $salesChannelContext
     * @return JsonResponse
     *
     * @throws \Shopware\Core\Content\Product\Exception\ProductNotFoundException
     */
    public function bundleOption(string $id, Request $request, SalesChannelContext $salesChannelContext) :JsonResponse
    {
        $switched = $request->get('switched');
        $options = $request->get('options');
        $result = $this->combinationFinder->find($id, $switched, $options, $salesChannelContext);

        return JsonResponse::create(['status' => 200, 'id' => $result->getVariantId()]);

    }
}

