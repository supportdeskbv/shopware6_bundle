<?php declare(strict_types=1);

namespace SupportDesk\BundleProduct\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\InheritanceUpdaterTrait;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1582194017Bundle extends MigrationStep
{
    use InheritanceUpdaterTrait;

    public function getCreationTimestamp(): int
    {
        return 1582194017;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS `bundle_product` (
              `id` BINARY(16) NOT NULL,
              `parent_id` BINARY(16) NOT NULL,
              `parent_version_id` BINARY(16) NOT NULL,
              `product_id` BINARY(16) NOT NULL,
              `product_version_id` BINARY(16) NOT NULL,
              `min_qty` INT(11) NOT NULL DEFAULT 0,
              `max_qty` INT(11) NULL,
              `default_qty` INT(11) NOT NULL DEFAULT 1,
              `sort_order` INT(11) NOT NULL DEFAULT 1,
              PRIMARY KEY (`id`),
              UNIQUE (`parent_id`, `product_id`),
              CONSTRAINT `fk.bundle_product.parent_id__parent_version_id` FOREIGN KEY (`parent_id`, `parent_version_id`)
                REFERENCES `product` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.bundle_product.product_id__product_version_id` FOREIGN KEY (`product_id`, `product_version_id`)
                REFERENCES `product` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $this->updateInheritance($connection, 'product', 'bundles');
    }

    public function updateDestructive(Connection $connection): void
    {
        $connection->executeUpdate('
            DROP TABLE IF EXISTS `bundle_product`
        ');
    }
}
