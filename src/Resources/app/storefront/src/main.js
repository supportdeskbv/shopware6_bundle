// src/Resources/app/storefront/src/main.js

// import all necessary storefront plugins
import BundlePlugin from './bundle-plugin/bundle-plugin.plugin';

// register them via the existing PluginManager
const PluginManager = window.PluginManager;
PluginManager.register('BundlePlugin', BundlePlugin, '[data-bundle-presenter]');