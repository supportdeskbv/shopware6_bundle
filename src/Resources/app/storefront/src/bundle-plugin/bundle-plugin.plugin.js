import Plugin from 'src/plugin-system/plugin.class';
import StoreApiClient from 'src/service/store-api-client.service';

/**
 * SupportDesk BundlePlugin - displays product bundles in storefront.
 * Initialization function is only evoked on product pages with active bundles.
 */
export default class BundlePlugin extends Plugin {

	init() {

		BundlePlugin.selectedOptions = [];
		BundlePlugin.optionGroups = $('*[data-property-group-id]');
		BundlePlugin.labelClass = '.product-bundle-configurator-option-label';
		BundlePlugin.labelSelectedClass = 'selected';
		BundlePlugin.loadingClass = 'loading';

		this._client = new StoreApiClient();

		this.disableBuyNowButton()

		this.setDefaultSelection()

		$('.product-bundle-configurator-option-label').on('click', (e) => {
			const productOption = e.target

			//Change the css class
			this.updateSelection($(productOption))

			//Create a form from all variant id's.
			this.updateForm()

		})

	}

	setDefaultSelection() {

		const component = this;

		component.startLoading();

		$.each($('*[data-property-group-id]'), function () {

			const $group = $(this);
			const label = $group.find(BundlePlugin.labelClass);
			const productOption = label[0];

			component.updateSelection($(productOption));
			component.getVariantId($(productOption))
			.then((data) => {
				component.handleResponse(data)
				component.stopLoading();
			})
			.catch(() => {
				component.stopLoading()
			});

		})

	}

	updateSelection(productOption) {

		const alreadySelected = productOption.parents('*[data-property-group-id]').find('.' + BundlePlugin.labelSelectedClass);
		alreadySelected.removeClass(BundlePlugin.labelSelectedClass);
		productOption.addClass(BundlePlugin.labelSelectedClass);

	}


	fetchRequiredVariants() {

		const promises = [];
		const component = this;

		$.each($('section[data-group-collection-id]'), function () {

			const $groupCollection = $(this);
			const $selectedOptions = $groupCollection.find('.product-bundle-configurator-option-label.' + BundlePlugin.labelSelectedClass);
			const data = {
				options: {},
				switched: component.getSwitchedVariantId()
			};
			const bundleProductId = $($selectedOptions[0]).data('bundle-product-id');

			data.options = component.createDataFromSelectedOptions($selectedOptions);

			const response = component.fetchVariantId(bundleProductId, data)

			promises.push(response)

		});

		return promises

	}

	updateForm() {

		const results = this.fetchRequiredVariants();
		const component = this;

		component.startLoading();

		Promise.all(results)
		.then((result) => {
			BundlePlugin.selectedOptions = result.map((r) => r.id);
			this.updateFormInput();
			component.stopLoading();
		})
		.catch(() => {
			component.stopLoading();
		});
	}

	getSwitchedVariantId() {
		// For now, it doesn't matter which variant id we use here.
		return BundlePlugin.selectedOptions[0] || '';
	}

	getVariantId(productOption) {

		const groupCollectionId = productOption.data('group-collection-id');
		const $groupCollection = $(`*[data-group-collection-id=${groupCollectionId}]`);
		const bundleProductId = productOption.data('bundle-product-id');
		const $groups = $groupCollection.find('*[data-property-group-id]');
		const $selectedOptions = $groupCollection.find('.product-bundle-configurator-option-label.' + BundlePlugin.labelSelectedClass);

		const data = {
			options: {},
			switched: this.getSwitchedVariantId()
		};

		if ($selectedOptions.length !== $groups.length) {
			//Should only get here during initialization
			return new Promise((resolve, reject) => {
				reject({status: 400});
			});
		}

		data.options = this.createDataFromSelectedOptions($selectedOptions);

		return this.fetchVariantId(bundleProductId, data)

	}

	fetchVariantId(bundleProductId, data) {
		return new Promise((resolve, reject) => {
			this._client.post(`/sales-channel-api/v1/bundleproduct/${bundleProductId}/options`, JSON.stringify(data), (response) => {
				try {
					response = JSON.parse(response);
					if (response.errors) {
						return reject(response.errors[0])
					}
					return resolve(response);
				} catch (e) {
					return reject(e);
				}
			})
		});
	}

	handleResponse(data) {

		BundlePlugin.selectedOptions.push(data.id)

		this.updateFormInput()

		if (this.allRequiredOptionsSelected()) {
			this.enableBuyNowButton()
		}

	}

	createLineItemFromVariantId(variantId) {
		return `
            <input type="hidden" name="lineItems[${variantId}][id]" value="${variantId}">
            <input type="hidden" name="lineItems[${variantId}][type]" value="product">
            <input type="hidden" name="lineItems[${variantId}][referencedId]" value="${variantId}">
            <input type="hidden" name="lineItems[${variantId}][stackable]" value="1">
            <input type="hidden" name="lineItems[${variantId}][removable]" value="1">
        `;
	}

	updateFormInput() {
		let html = '';
		for (const variantId of BundlePlugin.selectedOptions) {
			html += this.createLineItemFromVariantId(variantId);
		}
		$('#placeholder-selected-bundles').html(html);
	}

	allRequiredOptionsSelected() {
		return BundlePlugin.optionGroups.length === BundlePlugin.selectedOptions.length;
	}

	createDataFromSelectedOptions($selectedOptions) {

		const options = {}

		$.each($selectedOptions, function () {
			const $element = $(this);
			const groupId = $element.data('group-id');
			options[groupId] = $element.data('group-option-id');
		})

		return options

	}

	disableBuyNowButton() {
		$('#productDetailPageBuyProductForm button').attr('disabled', true)
	}

	enableBuyNowButton() {
		$('#productDetailPageBuyProductForm button').attr('disabled', false)
	}

	startLoading() {
		this.disableBuyNowButton()
		$('div[data-bundle-presenter]').addClass(BundlePlugin.loadingClass)
	}

	stopLoading() {
		this.enableBuyNowButton()
		$('div[data-bundle-presenter]').removeClass(BundlePlugin.loadingClass)
	}
}
