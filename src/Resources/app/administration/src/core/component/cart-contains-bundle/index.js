import template from './cart-contains-bundle.tml.twig';

const { Component } = Shopware;

Component.extend('cart-contains-bundle', 'sw-condition-base', {
    template
});
