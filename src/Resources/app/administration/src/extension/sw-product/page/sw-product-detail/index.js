const { Component } = Shopware;

import template from './sw-product-detail.html.twig'

Component.override('sw-product-detail', {
    template,
    computed: {
        productCriteria() {
            const criteria = this.$super('productCriteria');
            criteria.addAssociation('bundles');
            return criteria;
        },
    }
});
