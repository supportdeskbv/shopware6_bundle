import './view/sw-product-detail/bundleproduct-product-detail-bundle';
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

const {Module} = Shopware;

Module.register('supportdesk-bundleproduct', {
    // type: 'plugin',
    // name: 'bundleproduct',
    // title: 'bundleproduct.general.titleTextModule',
    // description: 'bundleproduct.general.descriptionTextModule',
    // version: '1.0.0',
    // targetVersion: '1.0.0',
    // color: '#57D9A3',
    // icon: 'default-symbol-products',
    // favicon: 'icon-module-products.png',
    // entity: 'product',

    routeMiddleware: (next, currentRoute) => {
        if (currentRoute.name === 'sw.product.detail') {
            currentRoute.children.push({
                name: 'bundleproduct.product.detail.bundle',
                path: '/sw/product/detail/:id/bundle',
                component: 'bundleproduct-product-detail-bundle',
                meta: {
                    parentPath: "sw.product.index"
                }
            });
        }

        next(currentRoute);
    }
});
