import template from './bundleproduct-product-detail-bundle.html.twig';

const {Component} = Shopware;

Component.register('bundleproduct-product-detail-bundle', {
    template,
    extends: {
        name: "sw-product-detail"
    }
});
