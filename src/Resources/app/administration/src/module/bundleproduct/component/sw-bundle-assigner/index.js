import template from './sw-bundle-assigner.html.twig'
import scss from "./sw-bundle-assigner.scss"

const {Component, Mixin} = Shopware;
const Criteria = Shopware.Data.Criteria;
const {mapState, mapGetters} = Component.getComponentHelper();

Shopware.Component.register('sw-bundle-assigner', {
    template,
    data() {
        return {
            bundles: [], //actual bundles (from bundle repository) with extra data
            products: [], //products that can be added as a bundle
            searchTerm: '',
        }
    },
    metaInfo() {
        return {
            title: 'Product bundles'
        }
    },
    created() {
        this.getBundles()
    },
    computed: {
        ...mapState('swProductDetail', [
            'productId'
        ]),
        ...mapGetters('swProductDetail', [
            'isLoading',
        ]),
    },
    methods: {
        getBundles(){

            Shopware.State.commit('swProductDetail/setLoading', ['product', true])

            this.getBundlesByProduct()
                .then((result) => {

                    this.bundles = result.length ? result.sort((a, b) => a.sortOrder - b.sortOrder): []

                    this.getProducts()
                        .then((result) => {
                            this.products = result

                            this.disableAlreadyBundledProducts()

                            Shopware.State.commit('swProductDetail/setLoading', ['product', false])

                        })
                        .catch((error) => {
                            Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                        })

                })

        },
        addToBundledProducts: function (product) {

            if (product.isBundled) {
                return
            }

            Shopware.State.commit('swProductDetail/setLoading', ['product', true])

            this.addToBundle(product)
                .then((data) => {
                    this.searchTerm = ''
                    this.getBundles()
                })
                .catch((error) => {
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                })

        },
        removeBundleFromProduct: function(bundle) {

            Shopware.State.commit('swProductDetail/setLoading', ['product', true])

            this.removeBundle(bundle)
                .then((data) => {
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                    this.getBundles()
                })
                .catch((e) => {
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                })

        },
        updateBundle: function(bundle) {
            Shopware.State.commit('swProductDetail/setLoading', ['product', true])

            this.saveBundle(bundle)
                .then((data) => {
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                    this.getBundles()
                })
                .catch((e) => {
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                })
        },
        disableAlreadyBundledProducts(){

            this.products.map((p) => {
                p.isBundled = !!(this.bundles.find((bp) => bp.product.id === p.id))
                return p
            })

        },
        increaseSortOrderForBundle: function(bundle){
            bundle.sortOrder++
            this.updateBundle(bundle)
        },
        decreaseSortOrderForBundle: function(bundle){
            bundle.sortOrder--
            this.updateBundle(bundle)
        },

        getBundlesByProduct(){

            return fetch(`${Shopware.Context.api.apiResourcePath}/fetch/bundle-product/?productId=${this.productId}`, {
                method: 'GET',
                headers: this.getHeaders(),
            })
                .then((response) => response.json())

        },

        getProducts(){

            return fetch(`${Shopware.Context.api.apiResourcePath}/fetch/product/`, {
                method: 'GET',
                headers: this.getHeaders(),
            })
                .then((response) => response.json())

        },

        addToBundle: function(product) {

            const data = {
                productId: product.id,
                parentProductId: this.productId,
                minQty: parseInt(product.productForm.minQty),
                maxQty: parseInt(product.productForm.maxQty),
                defaultQty: parseInt(product.productForm.defaultQty),
                sortOrder: parseInt(product.productForm.sortOrder)
            }

            return fetch(`${Shopware.Context.api.apiResourcePath}/add/bundle-product`, {
                method: 'post',
                headers: this.getHeaders(),
                body: JSON.stringify(data),
            })
                .then((response) => response.json())

        },

        search:function(){

            Shopware.State.commit('swProductDetail/setLoading', ['product', true])

            this.getSearchResult()
                .then((result) => {
                    this.products = result
                    this.disableAlreadyBundledProducts()
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                })
                .catch((e) => {
                    Shopware.State.commit('swProductDetail/setLoading', ['product', false])
                })

        },

        removeBundle: function(bundle) {
            const data = {
                id: bundle.id
            }

            return fetch(`${Shopware.Context.api.apiResourcePath}/remove/product/`, {
                method: 'post',
                headers: this.getHeaders(),
                body: JSON.stringify(data),
            })
                .then((response) => response.json())

        },

        saveBundle: function(bundle) {
            return fetch(`${Shopware.Context.api.apiResourcePath}/edit/bundle/`, {
                method: 'put',
                headers: this.getHeaders(),
                body: JSON.stringify(bundle),
            })
                .then((response) => response.json())

        },

        getSearchResult: function(){

            return fetch(`${Shopware.Context.api.apiResourcePath}/search/product/?searchTerm=${this.searchTerm}`, {
                method: 'GET',
                headers: this.getHeaders(),
            })
                .then((response) => response.json())

        },
        getHeaders: function(){
            return new Headers({
                'Authorization': `Bearer ${Shopware.Context.api.authToken.access}`,
                'Content-Type': 'application/json'
            })
        }
    }
})