<?php declare(strict_types = 1);

namespace SupportDesk\BundleProduct\Core\Content\Bundle\Command;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use SupportDesk\BundleProduct\Core\Content\Bundle\Aggregate\BundleProduct\BundleProductEntity;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BundleAssignCommand extends Command
{
    protected static $defaultName = 'bundle:assign';

    /**
     * @var EntityRepositoryInterface
     */
    protected $bundleRepository;

    /**
     * @var EntityRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        EntityRepositoryInterface $bundleRepository,
        EntityRepositoryInterface $productRepository
    ) {
        parent::__construct();

        $this->bundleRepository = $bundleRepository;
        $this->productRepository = $productRepository;
    }

    protected function configure(): void
    {
        parent::configure();
        $this->setName(static::$defaultName);
        $this->setDescription("Assigning sub products to bundle products");
        $this->addArgument('main_sku', InputArgument::REQUIRED, "Parent product number");
        $this->addArgument('sub_skus', InputArgument::IS_ARRAY, "Product number assignment (separated by space )");
        $this->addOption('append', 'a', null, 'Append the products instead of removing them');
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $io = new SymfonyStyle($input, $output);
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        $criteria->setLimit(50)
            ->addFilter(new EqualsAnyFilter('id', $input->getArgument('sub_skus')));

        $mainProductId = $this->productRepository->searchIds(
            (new Criteria())->addFilter(new EqualsFilter('id', $input->getArgument('main_sku'))),
            $context
        )->getIds();
        $productIds = $this->productRepository->searchIds($criteria, $context)->getIds();

        if (\count($productIds) === 0 || \count($mainProductId) === 0) {
            $io->error('Please create products before using bin/console bundle:assign');

            return 1;
        }

        $mainProductId = $mainProductId[0];
        $data = [];

        $defaults = $this->bundleRepository->getDefinition()->getDefaults();
        foreach ($productIds as $pid) {
            $data[] = array_merge($defaults, [
                'parentId' => $mainProductId,
                'productId' => $pid,
            ]);
        }

        if (!$input->getOption('append')) {
            $cleanIds = array_map(function(BundleProductEntity $row) {
                return ['id' => $row->getId(), 'parentId' => $row->getParentId(), 'productId' => $row->getProductId()];
            }, $this->bundleRepository->search(
                (new Criteria())->addFilter(new EqualsFilter('parentId', $mainProductId)),
                $context
            )->getElements());
            $this->bundleRepository->delete(array_values($cleanIds), $context);
        }

//        $this->bundleRepository->upsert($data, $context);
        $this->productRepository->update([["id" => $mainProductId, 'bundles' => $data]], $context);
        $io->success($mainProductId . ' has succesfully added/updated ' . count($productIds) . ' products');

        return null;
    }
}