<?php declare(strict_types=1);

namespace SupportDesk\BundleProduct\Core\Content\Bundle\Aggregate\BundleProduct;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class BundleProductEntity extends Entity
{
    use EntityIdTrait;

    protected $parent;
    protected $parentId;

    protected $product;
    protected $productId;

    protected $minQty;
    protected $maxQty;
    protected $defaultQty;
    protected $sortOrder;

    /**
     * @return string|null
     */
    public function getParentId(): ?string
    {
        return $this->parentId;
    }

    /**
     * @param string $parentId
     */
    public function setParentId(string $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return ProductEntity|null
     */
    public function getParent(): ?ProductEntity
    {
        return $this->parent;
    }

    /**
     * @param ProductEntity $parent
     */
    public function setParent(ProductEntity $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return ProductEntity|null
     */
    public function getProduct(): ?ProductEntity
    {
        return $this->product;
    }

    /**
     * @param ProductEntity $product
     */
    public function setProduct(ProductEntity $product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getMinQty(): ?int
    {
        return $this->minQty;
    }

    /**
     * @param mixed $minQty
     */
    public function setMinQty(int $minQty): void
    {
        $this->minQty = $minQty;
    }

    /**
     * @return mixed
     */
    public function getMaxQty(): ?int
    {
        return $this->maxQty;
    }

    /**
     * @param mixed $maxQty
     */
    public function setMaxQty(int $maxQty): void
    {
        $this->maxQty = $maxQty;
    }

    /**
     * @return mixed
     */
    public function getDefaultQty(): ?int
    {
        return $this->defaultQty;
    }

    /**
     * @param mixed $defaultQty
     */
    public function setDefaultQty(int $defaultQty): void
    {
        $this->defaultQty = $defaultQty;
    }

    /**
     * @return mixed
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param mixed $sortOrder
     */
    public function setSortOrder(int $sortOrder): void
    {
        $this->sortOrder = $sortOrder;
    }

}
