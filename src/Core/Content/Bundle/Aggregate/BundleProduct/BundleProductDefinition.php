<?php declare(strict_types = 1);

namespace SupportDesk\BundleProduct\Core\Content\Bundle\Aggregate\BundleProduct;

use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ReverseInherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\MappingEntityDefinition;

class BundleProductDefinition extends MappingEntityDefinition
{
    const ENTITY_NAME = 'bundle_product';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return BundleProductCollection::class;
    }

    public function getEntityClass(): string
    {
        return BundleProductEntity::class;
    }

    public function getDefaults(): array
    {
        return [
            'minQty' => 0,
            'maxQty' => 10,
            'defaultQty' => 1,
            'sortOrder' => 1,
        ];
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new PrimaryKey(), new Required()),
                (new FkField('parent_id', 'parentId', ProductDefinition::class))->addFlags(new Required()),
                (new ReferenceVersionField(ProductDefinition::class, 'parent_version_id'))->addFlags(new Required()),
                (new FkField('product_id', 'productId', ProductDefinition::class))->addFlags(new Required()),
                (new ReferenceVersionField(ProductDefinition::class, 'product_version_id'))->addFlags(new Required()),
                (new IntField('min_qty', 'minQty', 0)),
                (new IntField('max_qty', 'maxQty', 1)),
                (new IntField('default_qty', 'defaultQty', 1)),
                (new IntField('sort_order', 'sortOrder', 1)),
                (new ManyToOneAssociationField('parent', 'parent_id', ProductDefinition::class))
                    ->addFlags(new ReverseInherited('bundles')),
                new ManyToOneAssociationField('product', 'product_id', ProductDefinition::class),
            ]);
    }
}
