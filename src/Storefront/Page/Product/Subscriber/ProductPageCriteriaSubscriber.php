<?php declare(strict_types = 1);

namespace SupportDesk\BundleProduct\Storefront\Page\Product\Subscriber;

use Shopware\Core\Content\Product\Aggregate\ProductVisibility\ProductVisibilityDefinition;
use Shopware\Core\Content\Product\Exception\ProductNotFoundException;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Content\Product\SalesChannel\ProductAvailableFilter;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionCollection;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupDefinition;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Struct\ArrayEntity;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepositoryInterface;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Page\Product\ProductLoaderCriteriaEvent;
use Shopware\Storefront\Pagelet\Header\HeaderPageletLoadedEvent;
use SupportDesk\BundleProduct\Core\Content\Bundle\Aggregate\BundleProduct\BundleProductEntity;
use SupportDesk\BundleProduct\Core\Content\Product\ProductConfiguratorLoader;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductPageCriteriaSubscriber implements EventSubscriberInterface
{
    /**
     * @var SalesChannelRepositoryInterface
     */
    private $productRepository;
    /**
     * @var ProductConfiguratorLoader
     */
    private $pageConfiguratorLoader;

    public function __construct(
        SalesChannelRepositoryInterface $productRepository,
        ProductConfiguratorLoader $pageConfiguratorLoader
    ) {
        $this->productRepository = $productRepository;
        $this->pageConfiguratorLoader = $pageConfiguratorLoader;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ProductLoaderCriteriaEvent::class => 'onProductCriteriaLoaded',
            "sales_channel." . ProductEvents::PRODUCT_LOADED_EVENT => 'onProductLoaded',
        ];
    }

    /**
     * @param ProductLoaderCriteriaEvent $event
     *
     * @throws \Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException
     */
    public function onProductCriteriaLoaded(ProductLoaderCriteriaEvent $event): void
    {
        $event->getCriteria()
              ->addAssociation('bundles');


    }

    /**
     * @param EntityLoadedEvent $event
     *
     * @throws ProductNotFoundException
     * @throws \Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException
     */
    public function onProductLoaded(EntityLoadedEvent $event): void
    {
        $products = $event->getEntities();
        /** @var SalesChannelProductEntity $product */
        foreach ($products as $product) {
            $extension = $product->getExtension('bundles');
            if ($extension and $extension->getElements()) {
                /** @var BundleProductEntity $bundle */
                foreach ($extension->getElements() as $bundle) {
                    if (!$bundle->getProduct()) {
                        $bundleProduct = $this->loadBundleProduct($bundle, $event->getSalesChannelContext());
                        $bundleProduct->setSortedProperties(
                            $this->sortProperties($bundleProduct)
                        );
                        $bundleProduct->setParentId($bundleProduct->getId());
                        $bundleProduct->setCustomFields(
                            [
                                "configuratorCollection" => $this->pageConfiguratorLoader->load(
                                    $bundleProduct, $event->getSalesChannelContext()),
                                "defaultProduct" => $this->loadBundleProduct(
                                    $bundle, $event->getSalesChannelContext(), true)
                            ]);
                        $bundleProduct->setParentId(null);
                    }
                }
            }
        }
    }

    /**
     * @param BundleProductEntity $bundle
     * @param SalesChannelContext $context
     * @param bool                $loadFirstVariant
     *
     * @throws ProductNotFoundException
     * @throws \Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException
     */
    private function loadBundleProduct(BundleProductEntity $bundle, SalesChannelContext $context, bool $loadFirstVariant = false)
    {
        $bestVariant = $bundle->getProductId();
        if ($loadFirstVariant) {
            $bestVariant = $this->findBestVariant($bundle->getProductId(), $context);
        }
        $criteria = (new Criteria([$bestVariant]))
            ->addFilter(
                new ProductAvailableFilter(
                    $context->getSalesChannel()->getId(), ProductVisibilityDefinition::VISIBILITY_LINK))
            ->addAssociation('media')
            ->addAssociation('prices')
            ->addAssociation('option.group')
            ->addAssociation('option.media')
            ->addAssociation("configuratorSettings")
            ->addAssociation('cover')
            ->addAssociation('properties.group');
        $criteria->getAssociation('media')->addSorting(new FieldSorting('position'));
        $product = $this->productRepository->search($criteria, $context)
                                           ->get($bestVariant);
        if (!$product) {
            throw new ProductNotFoundException($bundle->getProductId());
        }
        if (!$loadFirstVariant) {
            $bundle->setProduct($product);
        }

        return $product;
    }

    /**
     * @throws InconsistentCriteriaIdsException
     */
    private function findBestVariant(string $productId, SalesChannelContext $salesChannelContext)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('product.parentId', $productId))
            ->addSorting(new FieldSorting('product.price'))
            ->setLimit(1);
        $variantId = $this->productRepository->searchIds($criteria, $salesChannelContext);
        if (\count($variantId->getIds()) > 0) {
            return $variantId->getIds()[0];
        }

        return $productId;
    }

    /**
     * Direct copy of ProductLoader::sortProperties
     *
     * @param ProductEntity $product
     *
     * @return PropertyGroupCollection
     */
    private function sortProperties(ProductEntity $product): PropertyGroupCollection
    {
        $properties = $product->getProperties();
        if ($properties === null) {
            return new PropertyGroupCollection();
        }
        $sorted = [];
        foreach ($properties as $option) {
            $group = $option->getGroup();
            if (!$group) {
                continue;
            }
            if (!$group->getOptions()) {
                $group->setOptions(new PropertyGroupOptionCollection());
            }
            $group->getOptions()->add($option);
            $sorted[$group->getId()] = $group;
        }
        usort(
            $sorted,
            static function (PropertyGroupEntity $a, PropertyGroupEntity $b)
            {
                return strnatcmp($a->getTranslation('name'), $b->getTranslation('name'));
            }
        );
        foreach ($sorted as $group) {
            $group->getOptions()->sort(
                static function (PropertyGroupOptionEntity $a, PropertyGroupOptionEntity $b) use ($group)
                {
                    if ($group->getSortingType() === PropertyGroupDefinition::SORTING_TYPE_ALPHANUMERIC) {
                        return strnatcmp($a->getTranslation('name'), $b->getTranslation('name'));
                    }
                    if ($group->getSortingType() === PropertyGroupDefinition::SORTING_TYPE_ALPHANUMERIC) {
                        return $a->getTranslation('name') <=> $b->getTranslation('name');
                    }

                    return $a->getPosition() <=> $b->getPosition();
                }
            );
        }

        return new PropertyGroupCollection($sorted);
    }
}
