<?php declare(strict_types=1);

namespace SupportDesk\BundleProduct\Controller;

use Shopware\Core\Content\Product\Aggregate\ProductVisibility\ProductVisibilityDefinition;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Api\Converter\ApiVersionConverter;
use Shopware\Core\Framework\Api\Serializer\JsonEntityEncoder;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"api"})
 */
class ApiController extends AbstractController
{
    private $apiVersionConverter;
    private $productRepository;
    private $encoder;
    private $bundleRepository;

    public function __construct(
        EntityRepositoryInterface $productRepository,
        EntityRepositoryInterface $bundleRepository,
        ApiVersionConverter $apiVersionConverter,
        JsonEntityEncoder $encoder
    ) {
        $this->productRepository = $productRepository;
        $this->bundleRepository = $bundleRepository;
        $this->apiVersionConverter = $apiVersionConverter;
        $this->encoder = $encoder;
    }

    /**
     * @Route("/api/v{version}/fetch/bundle-product", name="api.action.bundle_product.fetch_bundles", methods={"GET"})
     */
    public function fetchBundlesAction(Request $request, Context $context, int $version): JsonResponse
    {
        $productId = $request->get('productId');
        $criteria = new Criteria();
        $criteria->addAssociation('product');
        $criteria->setLimit(50)->addFilter(new EqualsFilter('parentId', $productId));
        /** @var EntitySearchResult $result */
        $result = $this->bundleRepository->search($criteria, Context::createDefaultContext());
        $decodedResult = $this->handleEntityResult($result, $version);

        return new JsonResponse($decodedResult);
    }

    /**
     * @Route("/api/v{version}/fetch/product", name="api.action.bundle_product.fetch_products", methods={"GET"})
     */
    public function fetchProductsAction(Request $request, Context $context, int $version)
    {

        $criteria = new Criteria();
        $criteria->setLimit(10);
        $result = $this->productRepository->search($criteria, Context::createDefaultContext());
        $decodedResult = $this->handleEntityResult($result);

        return new JsonResponse($decodedResult);
    }

    /**
     * @Route("/api/v{version}/search/product", name="api.action.bundle_product.search_products", methods={"GET"})
     */
    public function searchProductsAction(Request $request, Context $context, int $version)
    {
        $searchTerm = $request->get('searchTerm');
        $criteria = new Criteria();
        $criteria->setLimit(10);
        $criteria->addFilter(
            new MultiFilter(
                MultiFilter::CONNECTION_OR, [
                    new ContainsFilter('name', $searchTerm),
                    new ContainsFilter('productNumber', $searchTerm),
                ]
            )
        );
        $criteria->addAssociation('product.visibilities');
        $criteria->addFilter(
            new NotFilter(
                NotFilter::CONNECTION_AND, [new EqualsFilter('product.visibilities.visibility', ProductVisibilityDefinition::VISIBILITY_LINK)]
            )
        );

        /** @var ProductCollection $result */
        $result = $this->productRepository->search($criteria, Context::createDefaultContext());
        $decodedResult = $this->handleEntityResult($result);

        return new JsonResponse($decodedResult);
    }

    /**
     * @Route("/api/v{version}/add/bundle-product", name="api.action.bundle_product.add", methods={"POST"})
     */
    public function addAction(Request $request, Context $context): JsonResponse
    {

        $mainProductId = $request->get('parentProductId');
        $subProductId = $request->get('productId');
        $minQty = $request->get('minQty');
        $maxQty = $request->get('maxQty');
        $defaultQty = $request->get('defaultQty');
        $sortOrder = $request->get('sortOrder');
        $context = Context::createDefaultContext();

        $data = [[
                     'minQty'     => $minQty,
                     'maxQty'     => $maxQty,
                     'defaultQty' => $defaultQty,
                     'sortOrder'  => $sortOrder,
                     'parentId'   => $mainProductId,
                     'productId'  => $subProductId,
                 ]];

        $this->productRepository->update([["id" => $mainProductId, 'bundles' => $data]], $context);

        return new JsonResponse(['status' => 200]);
    }

    /**
     * @Route("/api/v{version}/remove/product/", name="api.action.bundle_product.remove_by_id", methods={"POST"})
     */
    public function removeBundleById(Request $request, Context $context, int $version)
    {

        $this->bundleRepository->delete(
            [
                ['id' => $request->get('id')],
            ],
            Context::createDefaultContext()
        );

        return new JsonResponse(['status' => 200]);
    }

    /**
     * @Route("/api/v{version}/edit/bundle/", name="api.action.bundle_product.edit", methods={"PUT"})
     */
    public function editBundle(Request $request, Context $context, int $version)
    {

        $minQty = (int)$request->get('minQty');
        $maxQty = (int)$request->get('maxQty');
        $defaultQty = (int)$request->get('defaultQty');
        $sortOrder = (int)$request->get('sortOrder');
        $id = $request->get('id');
        $parentId = $request->get('parentId');
        $mainProductId = $request->get('product')['id'];

        $criteria = new Criteria([$id]);
        $criteria->setLimit(1);
        $result = $this->bundleRepository->search($criteria, $context)->getEntities()->getElements();

        if (!$result) {
            return new JsonResponse(
                [
                    'status'  => 404,
                    'message' => 'Entity not found',
                ]
            );
        }

        $this->bundleRepository->upsert(
            [
                [
                    'id'         => $id,
                    'minQty'     => $minQty,
                    'maxQty'     => $maxQty,
                    'defaultQty' => $defaultQty,
                    'sortOrder'  => $sortOrder,
                    'productId'  => $mainProductId,
                    'parentId'   => $parentId,
                ],
            ],
            $context
        );

        return new JsonResponse(['status' => 200]);
    }

    protected function handleEntityResult(EntitySearchResult $result, int $version = 1)
    {

        $decoded = [];

        /** @var ProductEntity $entity */
        foreach ($result as $entity) {

            $decoded[] = array_merge(
                $this->apiVersionConverter->convertEntity(
                    $this->productRepository->getDefinition(),
                    $entity,
                    $version
                ),
                [
                    'productForm' => [
                        'showForm'   => false,
                        'minQty'     => 1,
                        'maxQty'     => 10,
                        'defaultQty' => 1,
                        'sortOrder'  => 1,
                    ],
                ]

            );
        }

        return $decoded;
    }

    protected function getApiBaseUrl(Request $request): string
    {
        $versionPart = $this->getVersion($request) ? ('/v' . $this->getVersion($request)) : '';

        return $this->getBaseUrl($request) . '/api' . $versionPart;
    }

    protected function getBaseUrl(Request $request): string
    {
        return $request->getSchemeAndHttpHost() . $request->getBasePath();
    }

    protected function getVersion(Request $request): int
    {
        return (int)$request->get('version');
    }
}
